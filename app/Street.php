<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Street extends Model {
    protected $fillable = [
        'kladr_id', 'name', 'zip', 'okato'
    ];

}