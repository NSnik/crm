<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Services\Kladr\Api;
use App\Services\Kladr\Query;
use App\Services\Kladr\Object;
use App\Services\Kladr\ObjectType;

//Models
use App\City;
use App\Street;
use Illuminate\Support\Facades\DB;

class KladrController extends Controller {
    private function getCities(){
        $serchBy = array("А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я");

        $cities = array();
        foreach ($serchBy as $serch) {
            $citiesBySearch = array();
            $countOffset = 0;

            do {
                // Инициализация api, в качестве параметров указываем токен и ключ для доступа к сервису
                $api = new Api('5807a8570a69de5b2e8b45a3', '');

                // Формирование запроса
                $query = new Query();
                $query->ContentName = $serch;
                $query->ContentType = ObjectType::City;

                $query->WithParent = FALSE;

                $query->Offset = $countOffset;

                // Получение данных в виде ассоциативного массива
                $citiesBySearch = $api->QueryToArray($query);

                if ($citiesBySearch) {
                    $countOffset += 400;
                    $onlyCities = array();
                    foreach ($citiesBySearch as $oneCity) {
                        if (isset($oneCity['id']) && isset($oneCity['name']) && $oneCity['id'] && $oneCity['name']) {
                            //только города без посёлков
                            if ($oneCity['typeShort'] == "г") {
                                $onlyCities[] = array(
                                    'kladr_id' => $oneCity['id'] ?: 0,
                                    'name' => (!empty($oneCity['typeShort']) && !empty($oneCity['name'])) ? $oneCity['typeShort'].' '.$oneCity['name'] : '',
                                    'zip' => $oneCity['zip'],
                                    'okato' => $oneCity['okato'],
                                    'from_kladr' => true
                                );
                            }
                        }
                    }

                    $cities = array_merge($cities, $onlyCities);
                }
//                echo $serch;
            } while($citiesBySearch);

        }

        //получаем только уникальные значения так ка входе поиска могли появится дубли
        $keys = array(); // Массив ключей, которые уже встречались
        foreach($cities as $k=>$val) {
            if(array_key_exists($val['kladr_id'],$keys)) {
                unset($cities[$k]);
            } else {
                $keys[$val['kladr_id']] = 1;
            }
        }

        return $cities;
    }

    private function getStreets($parentId){
        if (!$parentId)
            return array();

        //$serchBy = array("А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я");
        $countOffset = 0;

        $streets = array();
        do {
            $streetsBySearch = array();
            // Инициализация api, в качестве параметров указываем токен и ключ для доступа к сервису
            $api = new Api('5807a8570a69de5b2e8b45a3', '');

            // Формирование запроса
            $query              = new Query();
            $query->ParentType  = ObjectType::City;
            $query->ParentId    = $parentId;
            $query->ContentType = ObjectType::Street;

            $query->WithParent = FALSE;

            $query->Offset = $countOffset;

            // Получение данных в виде ассоциативного массива
            $streetsBySearch = $api->QueryToArray($query);
            if ($streetsBySearch) {
                $countOffset += 400;
                $onlyStreets = array();
				foreach ($streetsBySearch as $oneStreet){
                    if(isset($oneStreet['id']) && isset($oneStreet['name']) && $oneStreet['id'] && $oneStreet['name']) {
                            $onlyStreets[] = array(
                                'kladr_id' => $oneStreet['id'] ?: 0,
                                'name' => $oneStreet['name'] ?: '',
                                'zip' => $oneStreet['zip'],
                                'okato' => $oneStreet['okato'],
                                'from_kladr' => true
                            );
                    }
                }

                $streets = array_merge($streets, $onlyStreets);
            }
        } while ($streetsBySearch);

        //получаем только уникальные значения так ка входе поиска могли появится дубли
        $keys = array(); // Массив ключей, которые уже встречались
        foreach($streets as $k=>$val) {
            if(array_key_exists($val['kladr_id'],$keys)) {
                unset($streets[$k]);
            } else {
                $keys[$val['kladr_id']] = 1;
            }
        }

        return $streets;
    }

    public function parseData() {
        set_time_limit(10000);
//        echo 'start';

        $cities = $this->getCities();
        if ($cities) {
            //добавляем города в базу
            DB::table('cities')->insert($cities);

            $cities = DB::table('cities')->select('id', 'kladr_id')->get();
            foreach ($cities as $city) {
                if ($strets = $this->getStreets($city->kladr_id)) {
                    foreach ($strets as &$street) {
                        $street['city_id'] = $city->id;
                    }

                    DB::table('streets')->insert($strets);
                }
                //echo "City done:{$city->kladr_id} <br>";
            }
        }

        //echo ('All done');
        return;
    }

    public function updateData() {
        set_time_limit(75000);
        echo 'start';

        $cities = $this->getCities();
        if ($cities) {
            foreach ($cities as $city) {
                $dbCity = City::firstOrNew(['kladr_id' => $city['kladr_id']]);

                $dbCity->kladr_id = $city['kladr_id'] ?: 0;
                $dbCity->name = $city['name'] ?: '';
                $dbCity->zip = $city['zip'];
                $dbCity->okato = $city['okato'];
                $dbCity->from_kladr = true;

                $dbCity->save();
                if ($strets = $this->getStreets($city['kladr_id'])) {
                    foreach ($strets as $street) {
                        $dbStreet = Street::firstOrNew(['kladr_id' => $street['kladr_id']]);

                        //если есть объект то пропускаем
                        if (isset($dbStreet->id) && $dbStreet->id)
                            continue;

                        $dbStreet->city_id = $dbCity->id;
                        $dbStreet->kladr_id = $street['kladr_id']?: 0;
                        $dbStreet->name = $street['name'] ?: '';
                        $dbStreet->zip = $street['zip'];
                        $dbStreet->okato = $street['okato'];
                        $dbStreet->from_kladr = true;

                        $dbStreet->save();
                    }
                }
                echo 'City done';
            }
        }

        echo 'All done';
        return;
    }
}// class
