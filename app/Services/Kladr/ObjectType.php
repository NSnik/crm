<?php
/**
 * Created by PhpStorm.
 * User: Никита
 * Date: 20.10.2016
 * Time: 0:54
 */

namespace App\Services\Kladr;


/**
 * Перечисление типов объектов
 */
class ObjectType
{
    /**
     * Регион
     */
    const Region = 'region';

    /**
     * Район
     */
    const District = 'district';

    /**
     * Населённый пункт
     */
    const City = 'city';

    /**
     * Улица
     */
    const Street = 'street';

    /**
     * Строение
     */
    const Building = 'building';
}