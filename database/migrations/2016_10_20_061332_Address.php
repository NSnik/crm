<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Address extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kladr_id');
            $table->text('name');
            $table->integer('zip')->nullable();
            $table->bigInteger('okato')->nullable();
            $table->boolean('from_kladr')->nullable();
            $table->timestamps();
        });

        Schema::create('streets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('city_id')->unsigned()->index();
            $table->bigInteger('kladr_id');
            $table->text('name');
            $table->integer('zip')->nullable();
            $table->bigInteger('okato')->nullable();
            $table->boolean('from_kladr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('cities');
        Schema::drop('streets');
    }
}
